Based on this guide: http://start.jcolemorrison.com/building-an-angular-and-express-app-part-1/

## Installation
npm install -g express express-generator yo generator-angular bower nodemon

### Extras:
npm install -g phantomjs jasmine-core karma
npm install -g ruby compass
gem install compass

npm install grunt-contrib-imagemin



### Ruby on OSX:
If you get this: *ERROR: While executing gem ... (Gem::FilePermissionError) You don't have write permissions for the /Library/Ruby/Gems/2.0.0 directory*

The Solution
#### Step 1: Install Rbenv
* brew install rbenv ruby-build
* echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.zshrc
* echo 'eval "$(rbenv init -)"' >> ~/.zshrc


#### Step 2: Restart shell
* source ~/.zshrc

#### Step 3: Install Ruby, Set global and Rehash
* rbenv install 2.2.3
* rbenv global 2.2.3
* rbenv rehash

#### Step 4: gem install as usual
